# nfe-dotnet

Biblioteca .NET para auxílio na manipulação e emissão de NF-e (Notas Fiscais Eletrônicas)

## Idéia

Imaginamos um serviço assíncrono para emissão de NF-e com a estrutura a baixo, criamos essa
biblioteca para auxiliar a construção desses serviços.

### Emitir Nota Fiscal
Um sistema próprio de emissão de notas fiscais, deve guardar alguns dados da Nota Fiscal em
base local com formato adequado a sua operação.

Modelo Nota Fiscal:
```json
{
  "id": "",
  "outros": "..."
}
```

E para gerenciar a emissão específica de NF-e, temos um modelo específico para isso, ou seja
para controle da emissão da NF-e em si.

Modelo próprio NF-e:
```json
 {
  "ref": "idNotaFiscal",
  "authorization": "chaveAutorizacao",
  "content": "xml",
  "status": ["emitida", "assinada", "enviada", "autorizada", "erro"],
  "error_type": ["gerar", "assinar", "enviar", "processar"],
  "error_message": "",
  "error_details": ""
}
```

### Serviços auxiliares

Temos então alguns serviços auxiliares que executam etapas distintas da emissão
da NF-e, e é aqui que essa biblioteca auxiliar entra, ela ajuda a esses serviços
conseguirem executar suas operações.

#### 1) Serviço de geração de XML

Um serviço específico que obtem os dados próprios do sistema para Nota Fiscal, e
gera um XML de NF-e de acordo com as especificações documentadas.

**INPUT**: Nota Fiscal de sistema próprio<br>
**OUTPUT**: `NF-e("emitida")`, ou `error_type("gerar")`


#### 2) Serviço de assinatura

Um serviço específico para assinar o XML da NF-e.

**INPUT**: `NF-e("emitida")` + `certificate("pfx")`<br>
**OUTPUT**: `NF-e("assinada")`, ou `error_type("assinar")`

#### 3) Serviço de envio síncrono

Um serviço específico para envio de NF-e imediatas (NFC-e) síncronas.

**INPUT**: `NF-e("assinada")`<br>
**OUTPUT**: `NF-e("autorizada")`, ou `error_type("enviar")`

#### 4) Serviço de envio assíncrono

Um serviço específico para envio de NF-e assíncronas.

**INPUT**: `NF-e("assinada")`<br>
**OUTPUT**: `NF-e("enviada")`, ou `error_type("enviar")`

#### 5) Serviço de consulta assíncrona

Um serviço específico para consulta de status para NF-e que foram enviadas assincronamente.

**INPUT**: `NF-e("enviada")`<br>
**OUTPUT**: `NF-e("autorizada")`, ou `error_type("processar")`

